INSERT INTO `category` VALUES (1,'Base de données');
INSERT INTO `category` VALUES (2,'Gestion de projet');
INSERT INTO `user` VALUES (1,'Aurélien','Delorme','adelorme-ext@formateur-humanbooster.com');
INSERT INTO `articles` VALUES (1,'Utiliser le driver JDBC','2023-07-25 19:52:22','Aujoud\'hui nous allons utiliser une base de données relationelle',1,1);
INSERT INTO `articles` VALUES (2,'Utiliser un ORM','2021-07-12 19:53:15','Qu\'est ce qu\'un ORM ?',1,1);
INSERT INTO `articles` VALUES (3,'Aujourd\'hui nous avons appris à nous servir de JIRA','2023-07-25 19:53:42','JIRA est développé par Atlassian',2,1);
